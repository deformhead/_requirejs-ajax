define(

    function module( ) {

        'use strict' ;

        var Ajax ;

        Ajax = function Ajax( data, error, success ) {

            var initialize ;
            var request ;
            var watch ;

            initialize = function initialize( data, error, success ) {

                request = new XMLHttpRequest( ) ;

                // watch state changes
                request.onreadystatechange = watch.bind( this, error, success ) ;

                // perform ajax call
                request.open( data.method, data.url, true ) ;
                request.send( data.data || null ) ;

            // keep the context
            }.bind( this ) ;

            watch = function watch( error, success ) {

                // if request is done
                if ( request.readyState === XMLHttpRequest.DONE ) {

                    // if request is successful and if sucess callback is defined
                    if ( request.status === 200
                    && typeof ( success ) === 'function' ) {

                        // call user's success callback
                        success( request.responseText ) ;
                    }

                    // if request is successful and if error callback is defined
                    else if ( request.status !== 200
                    && typeof ( error ) === 'function' ) {

                        // call user's error callback
                        error( request.responseText ) ;
                    }
                }
            } ;

            // initialize this instance
            initialize( data, error, success ) ;
        } ;

        return ( Ajax ) ;
    }
) ;
